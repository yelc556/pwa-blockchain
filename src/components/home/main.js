import React, { Component } from 'react';


// components
import './main.css';
import Header from './header'
import Banner from './section'
import SectionOne from './sectionOne'
import Footer from './Footer'

class Main extends Component {
    render() {
        return (
            <div className="is-preload landing">
                <div id="page-wrapper">
                    <Header/>
                    <Banner/>
                    <SectionOne/>
                    <Footer/>
                </div>
            </div>
        );
    }
}

export default Main;
