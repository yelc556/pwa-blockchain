import React, { Component } from 'react';

export default class Header extends Component{
    render(){
        return (
            <header id="header">
                <h1 id="logo"><a href="#banner">BLOCKCHAIN</a></h1>
                <nav id="nav">
                    <ul>
                        <li><a href="/simulador">Simulador</a></li>
                        <li><a href="#one">Información</a></li>
                        <li><a href="#footer">Acerca de?</a></li>
                    </ul>
                </nav>
            </header>
        );
    }
}