import {Component} from "react";
import React from "react";

import LogoOne from '../../assets/img/sectionOne.jpg'


export default class SectionOne extends Component {
    render() {
        return (
            <section id="one" className="spotlight style1 bottom">
                <span className="image fit main"><img src={LogoOne} alt="red-blockchain"/></span>
                <div className="content">
                    <div className="container">
                        <div className="row">
                            <div className="col-4 col-12-medium">
                                <header>
                                    <h2>Blockchain</h2>
                                </header>
                            </div>
                            <div className="col-4 col-12-medium">
                                <p>Es una nueva tecnología conocida como cadena de bloques, su principal función es la de transferir cadenas de datos digitales con una codificación confiable, debido a que presenta características que la convierten el altamente seguras.  También se puede definir como un libro que contiene una serie de registros, el cual está distribuido en diferentes partes. </p>
                            </div>
                            <div className="col-4 col-12-medium">
                                <p>Un Blockchain es un controlador digital compartido, que muestra las manipulaciones y que registra las transacciones en redes “par a par” públicas o privadas. El distribuidor, que está distribuido en todos los nodos que pertenecen a la red, registra constantemente en una cadena secuencial de bloques de cifrado hash enlazado, que es el historial de intercambios de activos que ha tenido lugar entre los pares de la red.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="#footer" className="goto-next scrolly">Footer</a>
            </section>
        );
    }
}