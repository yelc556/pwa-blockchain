import React, { Component } from 'react';

import Logo from '../../assets/img/logo2.png'

export default class Banner extends Component{
    render(){
        return (
            <section id="banner">
                <div className="content">
                    <header>
                        <h2>BLOCKCHAIN</h2>
                        <p>Es un algoritmo de codificación que permite <br/>
                             transferir cadenas de datos digitales de manera <br/>
                             privada y segura.</p>
                    </header>
                    <span className="image"><img src={Logo} alt="logo-blockchain"/></span>
                </div>
                <a href="#one" className="goto-next scrolly">Next</a>
            </section>
        );
    }
}
