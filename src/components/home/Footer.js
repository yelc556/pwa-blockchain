import React, { Component } from 'react';

export default class Footer extends Component {
      render(){
          return(
              <footer id="footer">
                  <ul className="icons">
                      <li><a href="https://gitlab.com/yelc556/pwa-blockchain" className="icon alt fa-github"><span className="label">GitHub</span></a></li>
                  </ul>
                  <ul className="copyright">
                      <li>&copy; yelcDev</li>
                      <li>&copy; paoP</li>
                  </ul>
              </footer>
          );
    }
}