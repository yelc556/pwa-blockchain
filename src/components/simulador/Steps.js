import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import TextField from "@material-ui/core/TextField/TextField";
import CryptoJS from 'crypto-js'
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Info from '@material-ui/icons/Info';

const tutorialSteps = [
    {
        label: 'HASH SHA256',
        generar: false,
    },
    {
        label: 'HASH SHA256',
        subtitle: 'Dificultad de 2 ceros',
        generar: true,
    },
];

const styles = theme => ({
    root: {
        maxWidth: '75%',
        flexGrow: 1,
        background: 'white',
    },
    header: {
        display: 'flex',
        alignItems: 'center',
        height: 75,
        paddingLeft: theme.spacing.unit * 4,
    },
    textField: {
        marginBottom: 25,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    button: {
        margin: theme.spacing.unit,
    },
});

class TextMobileStepper extends React.Component {
    state = {
        activeStep: 0,
        open: false,
        scroll: 'paper',
        generar: true,
        difficulty: 2,
        data:''
    };

    handleClickOpen = scroll => () => {
        this.setState({ open: true, scroll });
    };

    handleClose = () => {
        this.setState({ open: false });
    };


    handleNext = () => {
        this.setState(prevState => ({
            multiline: '',
            data:'',
            activeStep: prevState.activeStep + 1,
            generar: false,
            nonce: 0,
        }));
    };

    handleBack = () => {
        this.setState(prevState => ({
            activeStep: prevState.activeStep - 1,
            generar: true,
            data:'',
            multiline: '',
        }));
    };

    handleChange = name => event => {
        this.setState({
            [name]: this.generateHash(event.target.value),
        });
    };


    isValidHashDifficulty(hash) {
        for (var i = 0; i < hash.length; i++) {
            if (hash[i] !== "0") {
                break;
            }
        }
        return i >= this.state.difficulty;
    }


    generateHash(data){
        if (data !== ''){
            if (this.state.generar)
                return CryptoJS.SHA256(data).toString();
            else {
                let nonce = 0;
                let timestamp = new Date().getTime();
                let cadena = CryptoJS.SHA256(data + timestamp).toString();
                while (!this.isValidHashDifficulty(cadena)) {
                    nonce += 1;
                    timestamp = new Date().getTime();
                    cadena = CryptoJS.SHA256(data + timestamp).toString();
                    console.table({'cadena': cadena, 'hash generado': nonce});
                }
                return cadena
            }
            return '';
        }
    }

    render() {
        const { classes, theme } = this.props;
        const { activeStep } = this.state;
        const maxSteps = tutorialSteps.length;

        return (
            <div className={classes.root}>
                <Paper square elevation={0} className={classes.header}>
                    <Typography variant="h4" gutterBottom color={"textPrimary"} onClick={this.handleClickOpen('paper')}>
                        <Info/> {tutorialSteps[activeStep].label}
                    </Typography>
                    <br/>
                    <Typography variant="subtitle1" gutterBottom color={"textPrimary"} onClick={this.handleClickOpen('paper')}>
                        { tutorialSteps[activeStep].subtitle}
                    </Typography>
                </Paper>
                <Paper>
                    <TextField
                        id="standard-multiline-flexible"
                        label="Datos"
                        multiline
                        rowsMax="4"
                        onChange={this.handleChange('multiline')}
                        className={classes.textField}
                        margin="normal"
                    />

                    <h6 className="align-center" id={"result"}>
                        {this.state.multiline}
                    </h6>
                </Paper>
                <MobileStepper
                    steps={maxSteps}
                    position="static"
                    activeStep={activeStep}
                    className={classes.mobileStepper}
                    nextButton={
                        <Button size="small" onClick={this.handleNext} disabled={activeStep === maxSteps - 1}>
                            Siguiente
                            {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                        </Button>
                    }
                    backButton={
                        <Button size="small" onClick={this.handleBack} disabled={activeStep === 0}>
                            {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                            Anterior
                        </Button>
                    }
                />
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    scroll={this.state.scroll}
                    aria-labelledby="scroll-dialog-title"
                >
                    <Typography variant={"h1"} id="scroll-dialog-title">Hash</Typography>
                    <DialogContent>
                        <Typography variant={"h6"}>
                            Es un algoritmo que se generan mediante una entrada ya sea con una contraseña, para determinados archivos, utiliza un algoritmo matemático para poder aumentar la seguridad y por medio de él se transforman cada uno de los datos ingresados, que se convierten en un conjunto de caracteres con una longitud fija, no importa la cantidad de datos ingresados, el valor del hash tendrá siempre la misma longitud
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cerrar
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

TextMobileStepper.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(TextMobileStepper);
