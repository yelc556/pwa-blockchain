import React, { Component } from 'react';
import {Link,} from "react-router-dom";

// components
import './simulador.css';
import TextMobileStepper from './Steps'
import Grid from "@material-ui/core/es/Grid/Grid";


class Simulador extends Component {
    render() {
        return (
            <div>
                <header id="header">
                    <h1 id="logo"><Link to={"/"}>BLOCKCHAIN</Link></h1>
                    <nav id="nav">
                        <ul>
                            <li><a href="#hash">Hash</a></li>
                            <li><a href="#bloques">Bloques</a></li>
                        </ul>
                    </nav>
                </header>

                <section id="hash">
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center">
                        <TextMobileStepper/>
                    </Grid>
                </section>
                <br/>
            </div>
        );
    }
}

export default Simulador;
