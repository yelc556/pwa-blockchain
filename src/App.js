// Dependencies
import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from "react-router-dom";


// Components
import Main from './components/home/main'
import Simulador from './components/simulador/simulador'

class App extends Component {
    render() {
        return (
            <div className="App">
                <Router>
                    <div>
                        <Route exact path="/" component={Main}/>
                        <Route path="/simulador" component={Simulador}/>
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;
